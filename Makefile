.PHONY: dev
dev:
	yarn dev

.PHONY: build
build:
	yarn build

.PHONY: start
start:
	yarn start

.PHONY: generate
generate:
	yarn generate
