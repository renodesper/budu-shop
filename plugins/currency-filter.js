import Vue from 'vue'

Vue.filter('dollar', function(value) {
  return `$${parseFloat(value).toFixed(2)}`
})

Vue.filter('rupiah', function(value) {
  let num_string = value.toString(),
    sisa = num_string.length % 3,
    rupiah = num_string.substr(0, sisa),
    ribuan = num_string.substr(sisa).match(/\d{3}/g),
    separator = ''

  if (ribuan) {
    separator = sisa ? '.' : ''
    rupiah += separator + ribuan.join('.')
  }

  return `Rp${rupiah}`
})
