import data from './static/storedata.json'

// const base = 'http://tokobudu.id'

let dynamicRoutes = () => {
  return new Promise((resolve) => {
    resolve(data.map((el) => `product/${el.id}`))
  })
}

export default {
  target: 'static',
  /*
   ** Headers of the page
   */
  head: {
    title: 'Budu Shop',
    script: [],
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      {
        hid: 'description',
        name: 'description',
        content: process.env.npm_package_description || '',
      },
      {
        name: 'facebook-domain-verification',
        content: '7dcd6lxaig74i9xelmat5iqqg1ivk7',
      },
    ],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }],
    link: [
      {
        rel: 'stylesheet',
        href:
          'https://fonts.googleapis.com/css?family=Montserrat:300,600|PT+Serif&display=swap',
      },
    ],
  },
  generate: {
    dir: 'public',
    routes: dynamicRoutes,
  },
  /*
   ** Customize the base url
   */
  // router: {
  //   base
  // },
  /*
   ** Customize the progress-bar color
   */
  loading: { color: '#fff' },
  /*
   ** Global CSS
   */
  css: ['normalize.css', { src: '~/assets/main.scss', lang: 'sass' }],
  /*
   ** Plugins to load before mounting the App
   */
  plugins: [`~/plugins/currency-filter.js`],
  /*
   ** Nuxt.js build modules
   */
  buildModules: [
    [
      '@nuxtjs/fontawesome',
      {
        component: 'fa',
        suffix: true,
        icons: {
          solid: true,
          brands: ['faFacebook', 'faInstagram'],
        },
      },
    ],
  ],
  /*
   ** Nuxt.js modules
   */
  modules: [
    [
      '@nuxtjs/gtm',
      {
        id: 'GTM-KLMJLWP',
      },
    ],
    [
      'nuxt-facebook-pixel-module',
      {
        track: 'PageView',
        pixelId: '398498508042083',
        autoPageView: true,
        disabled: false,
      },
    ],
  ],
  /*
   ** Build configuration
   */
  build: {
    /*
     ** You can extend webpack config here
     */
    extend(config, ctx) {},
  },
}
