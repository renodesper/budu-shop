# 🛍 Ecommerce Store with Netlify Functions and Stripe

> A serverless function to process stripe payments with Nuxt, Netlify, and Lambda

There are two articles explaining how this site is set up:
- Explanation of Netlify Functions and Stripe: [Let's Build a JAMstack E-Commerce Store with Netlify Functions](https://css-tricks.com/lets-build-a-jamstack-e-commerce-store-with-netlify-functions/)
- Explanation of dynamic routing in Nuxt for the individual product pages: [Creating Dynamic Routes in Nuxt Application](https://css-tricks.com/creating-dynamic-routes-in-a-nuxt-application/)

## Build Setup

``` bash
# install dependencies
$ yarn install

# serve with hot reload at localhost:3000
$ yarn dev

# build for production and launch server
$ yarn build
$ yarn start

# generate static project
$ yarn generate
```

## TODO

- Each image should be zoomed in when clicked
- Add ellipsis into product name
- Desktop version of the product page is a mess
- Fitur dan Kelebihan/Keuntungan dibuat berdampingan
